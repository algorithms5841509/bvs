#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <vector>
#include <algorithm>

#define PORT 8888

int countSetBits(char byte) {
    int count = 0;
    while (byte) {
        count += byte & 1;
        byte >>= 1;
    }
    return count;
}


int main() {
    int serverSocket, maxClients = 5;
    std::vector<int> clientSockets(maxClients, 0);
    fd_set readfds;
    int maxSocket;

    struct sockaddr_in address;
    int addrlen = sizeof(address);

    // Create a socket
    if ((serverSocket = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        std::cerr << "Socket creation error" << std::endl;
        return -1;
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    // Bind the socket to localhost:8888
    if (bind(serverSocket, (struct sockaddr *)&address, sizeof(address)) < 0) {
        std::cerr << "Bind failed" << std::endl;
        return -1;
    }

    // Listen for incoming connections
    if (listen(serverSocket, 4) < 0) {
        std::cerr << "Listen failed" << std::endl;
        return -1;
    }

    std::cout << "Server listening on port 8888..." << std::endl;

    while (true) {
        FD_ZERO(&readfds);
        FD_SET(serverSocket, &readfds);
        maxSocket = serverSocket;

        for (const auto &clientSocket : clientSockets) {
            if (clientSocket > 0) {
                FD_SET(clientSocket, &readfds);
                if (clientSocket > maxSocket) {
                    maxSocket = clientSocket;
                }
            }
        }

        // Wait for activity on sockets
        int activity = select(maxSocket + 1, &readfds, nullptr, nullptr, nullptr);
        if ((activity < 0) && (errno != EINTR)) {
            std::cerr << "Select error" << std::endl;
            return -1;
        }

        // New incoming connection
        if (FD_ISSET(serverSocket, &readfds)) {
            int clientSocket;
            if ((clientSocket = accept(serverSocket, (struct sockaddr *) &address, (socklen_t *) &addrlen)) < 0) {
                std::cerr << "Accept failed" << std::endl;
                return -1;
            }

            std::cout << "New connection, socket fd is " << clientSocket << std::endl;

            // Add new client socket to array
//            if (clientSockets.size() < maxClients) {
            clientSockets.push_back(clientSocket);
//            }

            // Send welcome message or do other initializations
            send(clientSocket, "Welcome to the server!\n", 22, 0);
        }

        // Check each existing client socket for activity
        for (int i = 0; i < clientSockets.size(); ++i) {
            int clientSocket = clientSockets[i];
            if (FD_ISSET(clientSocket, &readfds)) {
                char buffer[1024] = {0};
                int valread = read(clientSocket, buffer, 1024);

                if (valread <= 0) {
                    // Client disconnected
                    std::cout << "Client " << clientSocket << " disconnected" << std::endl;
                    close(clientSocket);
                    clientSockets.erase(std::remove(clientSockets.begin(), clientSockets.end(), clientSocket),
                                        clientSockets.end());
                } else {// Handle client message
                    std::string message(buffer);
                    std::cout << "Message from client " << clientSocket << ": " << message << std::endl;

                    // Count the number of '1's in the received message
                    int count = 0;
                    for (int i = 0; i < strlen(buffer); i++) {
                        count += countSetBits(buffer[i]);
                    }
                    std::string response = "Bits count: " + std::to_string(count);
                    send(clientSocket, response.c_str(), response.length(), 0);

                }
            }

        }
    }
    return 0;
}