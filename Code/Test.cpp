#include <iostream>
#include <pcap.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/ethernet.h>

void processPacket(u_char* args, const struct pcap_pkthdr* header, const u_char* packet) {
    struct ether_header* eth_header = (struct ether_header*)packet;

    // Извлекаем порт с которого пришло сообщение
    std::string incoming_interface = reinterpret_cast<char*>(args);
    std::cout << "Packet arrived on interface: " << incoming_interface << std::endl;

    // Здесь можно продолжить обработку пакета
}

int main() {
    char* dev; // Указатель на имя устройства, на котором будет осуществлен захват пакетов
    char errbuf[PCAP_ERRBUF_SIZE]; // Буфер для ошибок

    // Получаем имя устройства, через которое приходят пакеты
    dev = pcap_lookupdev(errbuf);
//    dev = "ens33";
    if (dev == nullptr) {
        std::cerr << "Couldn't find default device: " << errbuf << std::endl;
        return 1;
    }

    std::cout << "Device: " << dev << std::endl;

    // Открываем сетевой интерфейс для захвата пакетов
    pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
    if (handle == nullptr) {
        std::cerr << "Couldn't open device " << dev << ": " << errbuf << std::endl;
        return 1;
    }

    // Начинаем захватывать пакеты
    pcap_loop(handle, -1, processPacket, reinterpret_cast<u_char*>(dev));

    pcap_close(handle); // Закрываем сессию захвата пакетов
    return 0;
}