#include <iostream>
#include <fstream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <zconf.h>
#include <cstring>
#include <arpa/inet.h>
#include <csignal>

#define PORT 8887
#define BUFFER_SIZE 1024
static int s_sock = 0;
void sigint_handler(int signal) {
    // Ваш код для корректного завершения, например:
    std::string message;
    message = "bye";
    std::cout << "SIGINT received, exiting gracefully..." << std::endl;
    send(s_sock, message.c_str(), message.length(), 0);
    close(s_sock);
    exit(0);
}

int main() {
    int sock = 0;

    struct sockaddr_in serv_addr;
    char buffer[BUFFER_SIZE] = {0};

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        std::cerr << "Socket creation error" << std::endl;
        return -1;
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    serv_addr.sin_port = htons(PORT);

    if (connect(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        std::cerr << "Connection failed" << std::endl;
        return -1;
    }

    // Продолжаем общение с сервером после отправки файла
    while (true) {


        while (true) {
            memset(buffer, 0, BUFFER_SIZE);
            s_sock = sock;
            std::cout << "Enter your filename (type 'bye' to exit): ";
            std::string message;
            std::getline(std::cin, message);

            std::signal(SIGINT, sigint_handler);
            if (message == "bye") {
                send(sock, message.c_str(), message.length(), 0);
                close(sock);
                return 0;
            }
            std::ifstream outFile(message, std::ios::binary);
            if (!outFile.is_open()) {
                std::cerr << "File open error" << std::endl;
//                close(sock);
//                break;
            } else {
                outFile.seekg(0, std::ios::end);
                int outfile_size = outFile.tellg();
                outFile.seekg(0, std::ios::beg);

                char *outfile_buffer = new char[outfile_size];
                outFile.read(outfile_buffer, outfile_size);
                send(sock, message.c_str(), message.length(), 0);

                read(sock, buffer, BUFFER_SIZE);
                std::cout << "Server response: " << buffer << std::endl;
                std::cout << sock;

                send(sock, outfile_buffer, outfile_size, 0);
                memset(buffer, 0, BUFFER_SIZE);
                read(sock, buffer, BUFFER_SIZE);
                std::cout << "Server response: " << buffer << std::endl;
            }
        }



//        std::cout << sock;

    }

    close(sock);
    return 0;
}
