#include <iostream>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fstream>
#include <thread>
#include <cstring>
#include <vector>
#include <csignal>

int countSetBits(const std::string &content) {
    int count = 0;
    for (char c: content) {
        for (int i = 0; i < 8; ++i) {
            if ((c >> i) & 1) {
                count++;
            }
        }
    }
    return count;
}

void handleClient(int clientSocket) {
    char buffer[1024] = {0};
    std::string filename;
    bool shouldClose = false;

    while (!shouldClose) {
        memset(buffer, 0, sizeof(buffer));
        recv(clientSocket, buffer, 1024, 0);

        std::string receivedMessage(buffer);
        if (receivedMessage == "bye") {
            shouldClose = true;
            close(clientSocket);
            break;
        }

        filename = std::string(buffer);
        std::cout << "Received filename: " << filename << " to client " << clientSocket << std::endl;

        send(clientSocket, "File received. Please send file content.", 40, 0);

        char fileData[1024] = {0};
        recv(clientSocket, fileData, 1024, 0);

        std::string fileContent(fileData);

        int numSetBits = countSetBits(fileContent);
        std::string response = filename + " - " + std::to_string(numSetBits);

        send(clientSocket, response.c_str(), response.length() + 1, 0);
    }
    std::cout << "Клиент " << clientSocket << " отключился" << std::endl;

}
static int s_sock = 0;
void sigint_handler(int signal) {
    // Ваш код для корректного завершения, например:
    std::string message;
    message = "bye";
    std::cout << "SIGINT received, exiting gracefully..." << std::endl;
    send(s_sock, message.c_str(), message.length(), 0);
    close(s_sock);
    exit(0);
}
int main() {
    int serverSocket, clientSocket;
    struct sockaddr_in serverAddr, clientAddr;
    socklen_t addrSize = sizeof(struct sockaddr_in);

    serverSocket = socket(AF_INET, SOCK_STREAM, 0);

    if (serverSocket < 0) {
        std::cerr << "Error creating socket." << std::endl;
        return 1;
    }

    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = INADDR_ANY;
    serverAddr.sin_port = htons(8887);

    bind(serverSocket, (struct sockaddr *) &serverAddr, sizeof(serverAddr));

    listen(serverSocket, 5);
    std::vector<std::thread> clientThreads;
    while (true) {
        s_sock = serverSocket;
        std::signal(SIGINT, sigint_handler);
        clientSocket = accept(serverSocket, (struct sockaddr *) &clientAddr, &addrSize);
        if (clientSocket < 0) {
            continue;
        }

        std::cout << "Клиент подключен " << clientSocket << std::endl;

        std::thread clientThread(handleClient, clientSocket);
        clientThread.detach();
        clientThreads.push_back(std::move(clientThread));

    }
    close(serverSocket);
    return 0;
}