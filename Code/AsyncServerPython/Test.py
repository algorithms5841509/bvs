import asyncio
import socket
from asyncio import AbstractEventLoop
import signal


async def echo(connection: socket, loop: AbstractEventLoop) -> None:
    address = connection.getpeername()
    filename = None
    while data := await loop.sock_recv(connection, 1024):
        text = data.decode('utf-8')
        if text.lower() == 'bye':
            connection.close()
            print(f"Клиент {address} отключился")
            break
        if filename is None:
            # первое сообщение - это название файла
            filename = text
            print(f"Пользователь {address} получил данные из файла {filename}")
            connection.sendall(f"Отправлено".encode('utf-8'))
            # await asyncio.sleep(1)  # задержка в 1 секунду
        else:
            # последующие сообщения - это данные из файла
            total_bits = 0
            for char in text:
                binary = bin(ord(char))[2:]
                total_bits += binary.count('1')
            filename = None
            await loop.sock_sendall(connection, f'Total bits: {total_bits}\n'.encode('utf-8'))
            # await asyncio.sleep(1)  # задержка в 1 секунду


async def listen_for_connection(server_socket: socket,
                                loop: AbstractEventLoop):
    while True:
        connection, address = await loop.sock_accept(server_socket)
        connection.setblocking(False)
        print(f"Получен запрос на соединение от {address}")
        asyncio.create_task(echo(connection, loop))


async def main():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    server_address = ('127.0.0.1', 8887)
    server_socket.setblocking(False)
    server_socket.bind(server_address)
    server_socket.listen()

    # извлекаем цикл событий
    asyncio_loop = asyncio.get_event_loop()

    # добавляем обработчик на сигнал Ctrl+C
    loop = asyncio.get_event_loop()
    loop.add_signal_handler(signal.SIGINT, lambda: asyncio.create_task(loop.stop()))

    await listen_for_connection(server_socket, asyncio_loop)


asyncio.run(main())
