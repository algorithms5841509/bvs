import asyncio
import os


async def count_set_bits_async(filename):
    # with open(filename, 'rb') as file:
    #     content = file.read()
    binary = "".join(format(ord(char), "08b") for char in filename)
    return binary.count('1')


async def handle_client(reader, writer):
    client_address = writer.get_extra_info('peername')
    print(f"Подключение от {client_address}")

    while True:
        try:

            data = await reader.read(100)
            filename = data.decode()

            if filename.lower() == 'bye':
                print("Клиент отключен.")
                writer.close()
                break
            # if not os.path.exists(filename.split("/")[-1]):
            #     writer.write("Файл не найден.".encode())
            response = ''
            response = f"Файл {filename} получен"
            writer.write(response.encode())


            dataIn = await reader.read(100)
            file = dataIn.decode
            num_set_bits = await count_set_bits_async(file)
            response += f" - {num_set_bits}"
            writer.write(response.encode())

            await writer.drain()
        except ConnectionResetError as e:
            print(f"Клиент по порту {client_address} отключен.")
            # writer.close()
        except BrokenPipeError as e:
            print("Соединение с клиентом было разорвано")
            break
        # writer.close()


async def main():
    server = await asyncio.start_server(
        handle_client, "*", 8888
    )

    print("Сервер запущен и ожидает подключений...")

    async with server:
        await server.serve_forever()


if __name__ == "__main__":
    asyncio.run(main())
