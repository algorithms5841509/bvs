#include "swtch.h" // включаем заголовочный файл с определениями структур и функций

std::mutex mtx; // объявляем мьютекс для синхронизации доступа к общим ресурсам


void printTable(std::unordered_map<std::string, Entry> *table) { // объявляем функцию для вывода таблицы на экран
    std::cout << table->size() << std::endl; // выводим размер таблицы
    for (const auto &pair: *table) { // перебираем все элементы таблицы
        std::cout << "Mac: " << pair.first << ", Port: " << pair.second.eth << std::endl; // выводим MAC-адрес и номер порта для каждого элемента
    }
}

void clearTable(std::unordered_map<std::string, Entry> *table) { // объявляем функцию для очистки таблицы от устаревших записей
    while (true) { // бесконечный цикл
        time_t currentTime = std::time(nullptr); // получаем текущее время
        std::cout << currentTime << "tt\n"; // выводим текущее время
        for (auto it = table->begin(); it != table->end();) { // перебираем все элементы таблицы
            std::cout << it->second.timestamp << std::endl; // выводим время последнего обновления для каждого элемента
            if (currentTime - it->second.timestamp > 60) { // проверяем, не устарела ли запись
                std::cout << "запись о " << it->first << " устарела и была удалена\n"; // выводим сообщение об удалении
                it = table->erase(it); // удаляем элемент из таблицы
            } else {
                ++it; // переходим к следующему элементу
            }
        }
        std::this_thread::sleep_for(std::chrono::seconds(60)); // приостанавливаем выполнение на 60 секунд
    }
}

unsigned short compute_tcp_checksum(struct iphdr *pIph, unsigned short *ipPayload) {
    register unsigned long sum = 0;
    unsigned short tcpLen = ntohs(pIph->tot_len) - (pIph->ihl<<2);
    struct tcphdr *tcphdrp = (struct tcphdr*)(ipPayload);
//    tcphdrp ->ack_seq -= 1;
    //add the pseudo header
    //the source ip
    sum += (pIph->saddr>>16)&0xFFFF;
    sum += (pIph->saddr)&0xFFFF;
    //the dest ip
    sum += (pIph->daddr>>16)&0xFFFF;
    sum += (pIph->daddr)&0xFFFF;
    //protocol and reserved: 6
    sum += htons(IPPROTO_TCP);
    //the length
    sum += htons(tcpLen);

    //add the IP payload
    //initialize checksum to 0
    printf("Old %d ", tcphdrp->check);
    tcphdrp->check = 0;
    while (tcpLen > 1) {
        sum += * ipPayload++;
        tcpLen -= 2;
    }
    //if any bytes left, pad the bytes and add
    if(tcpLen > 0) {
        //printf("+++++++++++padding, %dn", tcpLen);
        sum += ((*ipPayload)&htons(0xFF00));
    }
    //Fold 32-bit sum to 16 bits: add carrier to result
    while (sum>>16) {
        sum = (sum & 0xffff) + (sum >> 16);
    }
    sum = ~sum;
    //set computation result
    tcphdrp->check = (unsigned short)sum;
    printf("New %d ", (unsigned short)sum);
}
void sendPacket(pcap_t *p, const u_char *buf, int size, char *dev) {
    // Create a mutable copy of the packet
    u_char *packet = new u_char[size];
    memcpy(packet, buf, size);

    struct tcphdr *tcp_header = const_cast<tcphdr *>((const struct tcphdr *) (packet + sizeof(struct ether_header) +
                                                                              sizeof(struct ip)));
    if (tcp_header->ack) {
        // Изменяем номер ACK
        uint32_t sequenceNumber = ntohl(tcp_header->ack_seq);
        if (sequenceNumber > 0) {
            std::printf("%d", sequenceNumber);
            tcp_header->ack_seq = htonl(sequenceNumber);

            // Recalculate the TCP checksum
            struct iphdr *ip_header = const_cast<iphdr *>((const struct iphdr *) (packet + sizeof(struct ether_header)));
            compute_tcp_checksum(ip_header, reinterpret_cast<unsigned short*>(tcp_header));

            std::printf("New %d\n\n", tcp_header->check);
        }
    }


    // Send the packet
    if (pcap_inject(p, packet, size) == -1) {
        fprintf(stderr, "Couldn't send pocket on device: %s\n", dev);
        exit(1);
    }

    // Free the memory
    delete[] packet;
}




void packetHandlerCallback(u_char *args, const struct pcap_pkthdr *pkthdr, const unsigned char *packet) { // объявляем функцию для обработки входящих пакетов
    auto cd = (CallbackData *) args; // преобразуем указатель на аргументы в указатель на CallbackData
    auto commutationTable = cd->MAC_table; // получаем указатель на таблицу коммутации

    const struct ether_header *ether;
    ether = (struct ether_header *) packet; // packet указывает на первый байт раздела данных содержащихся в пакете, который был захвачен

    std::string mac_source_str(ether_ntoa((const struct ether_addr *) ether->ether_shost)); // преобразуем MAC-адрес источника в строку
    std::string mac_dest_str(ether_ntoa((const struct ether_addr *) ether->ether_dhost)); // преобразуем MAC-адрес назначения в строку

    mtx.lock(); // блокируем мьютекс для синхронизации доступа к общим ресурсам

    fprintf(stdout, "ethernet header source: [%s]\n", mac_source_str.c_str()); // выводим MAC-адрес источника
    fprintf(stdout, "destination: [%s]\n", mac_dest_str.c_str()); // выводим MAC-адрес назначения

    auto it_dest = commutationTable->find(mac_dest_str); // ищем элемент в таблице коммутации по MAC-адресу назначения
    if (it_dest != commutationTable->end()) { // проверяем, найден ли элемент
        if (strcmp(it_dest->second.eth, cd->interface) != 0) { // проверяем, нет ли цикла в сети
            sendPacket(cd->other_interface_handler, packet,
                       pkthdr->len, cd->other_interface); // отправляем пакет на другое сетевое устройство
        }
    } else {
        sendPacket(cd->other_interface_handler, packet,
                   pkthdr->len,
                   cd->other_interface); // отправляем пакет на другое сетевое устройство, если элемента в таблице коммутации нет
    }

    auto it_source = commutationTable->find(mac_source_str); // ищем элемент в таблице коммутации по MAC-адресу источника
    if (it_source != commutationTable->end()) { // проверяем, найден ли элемент
        printf("Element with this source found in table\n"); // выводим сообщение
    } else {
        printf("Element with this source not found in table and be added [%s]\n",
               mac_source_str.c_str()); // выводим сообщение и добавляем элемент в таблицу коммутации
        Entry tmp{cd->interface, pkthdr->ts.tv_sec};
        commutationTable->insert({mac_source_str, tmp});
    }

    printTable(commutationTable); // выводим таблицу коммутации на экран

    mtx.unlock(); // разблокируем мьютекс для синхронизации доступа к общим ресурсам
}

void portCapture(u_char *cd) { // объявляем функцию для захвата пакетов на сетевом устройстве
    auto pCallbackData = (CallbackData *) cd; // преобразуем указатель на аргументы в указатель на CallbackData

    pcap_loop(pCallbackData->interface_handler, 0, packetHandlerCallback, (u_char *) cd); // начинаем захват пакетов и передаем указатель на функцию-коллбэк

    pcap_close(pCallbackData->interface_handler); // закрываем дескриптор сетевого устройства
}
