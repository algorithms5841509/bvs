#include "swtch.h"

//void updateMacTable(const std::string &macAddress, std::string eth) {
//    // Проверяем, есть ли уже запись с таким MAC-адресом в таблице
//    if (macTable.find(macAddress) != macTable.end()) {
//        // Если есть, обновляем порт и время
//        macTable[macAddress].eth = eth;
//        macTable[macAddress].timestamp = std::time(nullptr);
//    } else {
//        // Если нет, добавляем новую запись
//        Entry newEntry;
//        newEntry.macAddress = macAddress;
//        newEntry.eth = eth;
//        newEntry.timestamp = std::time(nullptr);
//        macTable[macAddress] = newEntry;
//    }
//}
//
//void decreaseAckNumber(u_char *packet) {
//    struct ether_header *eth_header = (struct ether_header *) packet;
//    int ether_type = ntohs(eth_header->ether_type);
//
//    // Проверяем, является ли пакет IP
//    if (ether_type == ETHERTYPE_IP) {
//        struct ip *ip_header = (struct ip *) (packet + sizeof(struct ether_header));
//
//        // Проверяем, является ли пакет TCP
//        if (ip_header->ip_p == IPPROTO_TCP) {
//            // Получаем заголовок TCP
//            struct tcphdr *tcp_header = (struct tcphdr *) (packet + sizeof(struct ether_header) + sizeof(
//                    struct ip *));
//
//            // Уменьшаем номер подтверждения на 1
//            tcp_header->th_ack = ntohl(ntohl(tcp_header->th_ack) - 1);
//        }
//    }
//}

//void processPacket(u_char *args, const struct pcap_pkthdr *header, const u_char *packet) {
//    // Получаем заголовок Ethernet
//    struct ether_header *eth_header = (struct ether_header *) packet;
//
//    std::string incoming_interface = reinterpret_cast<char *>(args);
//    std::cout << "Packet arrived on interface: " << incoming_interface << std::endl;
//
//    // Извлекаем MAC-адреса и порты
//    std::string source_mac = ether_ntoa((struct ether_addr *) &eth_header->ether_shost);
//    std::string dest_mac = ether_ntoa((struct ether_addr *) &eth_header->ether_dhost);
//    updateMacTable(source_mac, incoming_interface);
//    //decreaseAckNumber((u_char *) packet);
//
//    std::string outgoing_interface;
//    if (macTable.find(dest_mac) != macTable.end()) {
//        outgoing_interface = macTable[dest_mac].eth;
//    } else {
//        // Если целевого MAC-адреса нет в таблице, отправляем пакет во все подсети, кроме входящей
//        for (const auto &entry : macTable) {
//            if (entry.second.eth != incoming_interface) {
//                outgoing_interface = entry.second.eth;
//                break;
//            }
//        }
//    }
//    // Отправляем пакет на целевой интерфейс
//    if (!outgoing_interface.empty()) {
//        // Изменяем заголовок Ethernet
//        struct ether_addr *source_mac_ptr = (struct ether_addr *) &eth_header->ether_shost;
//        struct ether_addr *dest_mac_ptr = (struct ether_addr *) &eth_header->ether_dhost;
//    }
//}
//void processPacket(u_char *args, const struct pcap_pkthdr *header, const u_char *packet) {
//    char errbuf[PCAP_ERRBUF_SIZE];
//    // Получаем заголовок Ethernet
//    struct ether_header *eth_header = (struct ether_header *) packet;
//
//    std::string incoming_interface = reinterpret_cast<char *>(args);
//    std::cout << "Packet arrived on interface: " << incoming_interface << std::endl;
//
//    // Извлекаем MAC-адреса и порты
//    std::string source_mac = ether_ntoa((struct ether_addr *) &eth_header->ether_shost);
//    std::string dest_mac = ether_ntoa((struct ether_addr *) &eth_header->ether_dhost);
//
//    // Обновляем таблицу MAC-адресов
//    updateMacTable(source_mac, incoming_interface);
//
//    // Определяем целевой интерфейс
//    std::string outgoing_interface;
//    if (macTable.find(dest_mac) != macTable.end()) {
//        outgoing_interface = macTable[dest_mac].eth;
//    } else {
//        // Если целевого MAC-адреса нет в таблице, отправляем пакет во все подсети, кроме входящей
//        for (const auto &entry : macTable) {
//            if (entry.second.eth != incoming_interface) {
//                outgoing_interface = entry.second.eth;
//                break;
//            }
//        }
//    }
//
//    // Отправляем пакет на целевой интерфейс
//    if (!outgoing_interface.empty()) {
//        // Изменяем заголовок Ethernet
//        struct ether_addr *source_mac_ptr = (struct ether_addr *) &eth_header->ether_shost;
//        struct ether_addr *dest_mac_ptr = (struct ether_addr *) &eth_header->ether_dhost;
//        memcpy(dest_mac_ptr, source_mac_ptr, sizeof(struct ether_addr));
//        memcpy(source_mac_ptr, ether_aton(incoming_interface.c_str()), sizeof(struct ether_addr));
//
//        // Отправляем пакет на целевой интерфейс
//        pcap_t *outgoing_handle = pcap_open_live(outgoing_interface.c_str(), BUFSIZ, 1, 2, errbuf);
//        if (outgoing_handle != nullptr) {
//            pcap_sendpacket(outgoing_handle, packet, header->len);
//            pcap_close(outgoing_handle);
//        }
//    }
//}


int main(int argc, char *argv[]) {
    if (argc != 3){
        printf("Используйте: %s <eth1> <eth2>", argv[0]);
        return 0;
    }
    char *dev1, *dev2;
    //Сначала выбираем устройство для анализа пакетов
    dev1 = argv[1];
    dev2 = argv[2];
    auto MAC_table = new std::unordered_map<std::string, Entry>;
    

    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t *handle1, *handle2; // дескрипторы сессии захвата трафика

    handle1 = pcap_open_live(dev1, BUFSIZ, 1, 300, errbuf); // создание сессий захвата
    if (handle1 == nullptr) {
        fprintf(stderr, "Couldn't open device %s: %s\n", dev1, errbuf);
        return 1;
    }
    handle2 = pcap_open_live(dev2, BUFSIZ, 1, 300, errbuf); // создание сессий захвата
    if (handle2 == nullptr) {
        fprintf(stderr, "Couldn't open device %s: %s\n", dev2, errbuf);
        return 1;
    }

    auto *cd1 = new CallbackData{dev1, dev2, handle1, handle2, MAC_table};
    auto *cd2 = new CallbackData{dev2, dev1, handle2, handle1, MAC_table};
    // Устанавливаем захват только входящих пакетов
    if (pcap_setdirection(handle1, PCAP_D_IN) == -1) {
        std::cerr << "Could not set direction for capturing" << std::endl;
        pcap_close(handle1);
        return 1;
    }
    // Устанавливаем захват только входящих пакетов
    if (pcap_setdirection(handle2, PCAP_D_IN) == -1) {
        std::cerr << "Could not set direction for capturing" << std::endl;
        pcap_close(handle2);
        return 1;
    }
    std::thread port1(portCapture, (u_char *) cd1);
    std::thread port2(portCapture, (u_char *) cd2);
    std::thread tableProcessing(clearTable,MAC_table);
    port1.join();
    port2.join();
    tableProcessing.join();
    delete cd1;
    delete cd2;
    delete MAC_table;

    return 0;

}
