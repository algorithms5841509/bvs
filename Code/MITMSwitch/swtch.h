#ifndef BVS_SWTCH_H
#define BVS_SWTCH_H

#include <iostream>
#include <thread>
#include <mutex>
#include <unordered_map>
#include <sys/time.h>
#include <pcap.h>
#include <netinet/ether.h>
#include <unistd.h>
#include <cstring>
#include <netinet/ip.h>
#include <netinet/tcp.h>


struct Entry {
    char* eth;
    time_t timestamp; // Время создания записи
};
struct CallbackData {
    char *interface;
    char *other_interface;
    pcap_t *interface_handler;
    pcap_t *other_interface_handler;
    std::unordered_map<std::string, Entry> *MAC_table;
};

void printTable(std::unordered_map<std::string, Entry> *table);

void clearTable(std::unordered_map<std::string, Entry> *table);

void sendPacket(pcap_t *p, const u_char *buf, int size,char *dev);

void packetHandlerCallback(u_char *args, const struct pcap_pkthdr *pkthdr, const unsigned char *packet);

void portCapture(u_char *args);

#endif //BVS_SWTCH_H
